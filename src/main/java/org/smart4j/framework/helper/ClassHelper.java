package org.smart4j.framework.helper;

import org.smart4j.framework.annotation.Controller;
import org.smart4j.framework.annotation.Service;
import org.smart4j.framework.util.ClassUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Jungle
 * @created 2017/5/21 20:47
 */
public class ClassHelper {
	private static final Set<Class<?>> CLASS_SET;
	static {
		String basePackage = ConfigHelper.getAppBasePackage();
		CLASS_SET = ClassUtil.getClassSet(basePackage);
	}

	public static Set<Class<?>> getClassSet(){
		return CLASS_SET;
	}
	public static Set<Class<?>> getServiceClassSet(){
		HashSet<Class<?>> classSet = new HashSet<Class<?>>();
		for(Class<?> cls:CLASS_SET){
			if(cls.isAnnotationPresent(Service.class)){
				classSet.add(cls);
			}
		}
		return classSet;
	}

	public static Set<Class<?>> getControllerClassSet(){
		HashSet<Class<?>> classSet = new HashSet<Class<?>>();
		for(Class<?> cls:CLASS_SET){
			if(cls.isAnnotationPresent(Controller.class)){
				classSet.add(cls);
			}
		}
		return classSet;
	}

	public static Set<Class<?>> getBeanClassSet(){
		HashSet<Class<?>> classSet = new HashSet<Class<?>>();
		classSet.addAll(getControllerClassSet());
		classSet.addAll(getServiceClassSet());
		return classSet;
	}
}
