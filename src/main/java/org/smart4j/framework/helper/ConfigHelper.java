package org.smart4j.framework.helper;

import jdk.nashorn.internal.runtime.regexp.joni.Config;
import org.smart4j.framework.ConfigConstant;
import org.smart4j.framework.util.PropsUtil;

import java.util.Properties;

/**
 * @author Jungle
 * @created 2017/5/14 17:44
 */
public class ConfigHelper {
	private static final Properties CONFIG_PROPS = PropsUtil.loadProps(ConfigConstant.CONFIG_FILE);

	public static String getJdbcDriver(){
		return PropsUtil.getString(CONFIG_PROPS,ConfigConstant.JDBC_DRIVER);
	}

	public static String getJdbcUrl(){
		return PropsUtil.getString(CONFIG_PROPS,ConfigConstant.JDBC_URL);
	}

	public static String getJdbcUsername(){
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_USERNAME);
	}

	public static String getJdbcPassword(){
		return PropsUtil.getString(CONFIG_PROPS,ConfigConstant.JDBC_PASSWORD);
	}

	public static String getAppBasePackage()
	{
		return PropsUtil.getString(CONFIG_PROPS,ConfigConstant.APP_BASE_PACKAGE);
	}

	public static String getAppJspPath(){
		return PropsUtil.getString(CONFIG_PROPS,ConfigConstant.APP_JSP_PATH);
	}

	public static String getAppAssetPath(){
		return PropsUtil.getString(CONFIG_PROPS,ConfigConstant.APP_ASSET_PATH);
	}
}
