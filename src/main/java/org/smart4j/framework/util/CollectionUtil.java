package org.smart4j.framework.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import java.util.Collection;
import java.util.Map;

/**
 * @author Jungle
 * @created 2017/5/9 23:13
 */
public class CollectionUtil {
	public static boolean isEmpty(Collection<?> collection)
	{
		return CollectionUtils.isEmpty(collection);
	}
	public static boolean isEmpty(Map<?,?> map)
	{
		return MapUtils.isEmpty(map);
	}
}
